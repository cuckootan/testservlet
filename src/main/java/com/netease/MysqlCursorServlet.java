package com.netease;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-12.
 */
public class MysqlCursorServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("read mysql with cursor");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();

        try
        {
            properties.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String driver = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        Connection conn = null;
        PreparedStatement ptmt = null;
        ResultSet rs = null;

        try
        {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        try
        {
            conn = DriverManager.getConnection(url, username, password);

            String sql = "SELECT * from users";
            ptmt = conn.prepareStatement(sql);
            ptmt.setFetchSize(1);
            rs = ptmt.executeQuery();

            while (rs.next())
                System.out.println("Hello " + rs.getString("username"));

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (rs != null)
                    rs.close();
                if (ptmt != null)
                    ptmt.close();
            }
            catch (SQLException e)
            {
                // ignore.
            }
        }

        super.init();
    }
}
