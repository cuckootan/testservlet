package com.netease;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-9.
 */
public class PostServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String name = req.getParameter("name");
        String pw = req.getParameter("password");

        out.println("<HTML>");
        out.println("   <HEAD><TITLE>A Post Servlet</TITLE></HEAD>");
        out.println("   <BODY>");
        out.println("   调用doGet方法");
        out.println("<br></br>");
        out.println("用户名：" + name);
        out.println("<br></br>");
        out.println("密码：" + pw);
        out.println("   </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        System.out.println("Post Request");
        super.service(req, resp);
    }
}
