package com.netease;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-12.
 */
public class MysqlAntiInjectionServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("mysql anti injection");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();

        try
        {
            properties.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String driver = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        Connection conn = null;
        PreparedStatement ptmt = null;
        ResultSet rs = null;

        try
        {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        try
        {
            conn = DriverManager.getConnection(url, username, password);

            // ? 为占位符。
            // 一般方法都是将字段连接到 sql 命令中，然后进行编译执行。
            // 而这种方法则是先对这个 sql 命令进行编译，然后再进行字段替换，最后执行。可以有效地避免一般的 sql 注入。
            ptmt = conn.prepareStatement("SELECT * from users WHERE username = ? and password = ?");
            // 第一个占位符。
            ptmt.setString(1, "王刚");
            // 第二个占位符。
            ptmt.setString(2, "123456");
            rs = ptmt.executeQuery();

            // 如果返回的结果里有记录，则存在；否则不存在。
            if (rs.next())
                System.out.println("exist");
            else
                System.out.println("not exist");

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (ptmt != null)
                    ptmt.close();
                if (rs != null)
                    rs.close();
            }
            catch (SQLException e)
            {
                // ignore.
            }
        }

        super.init();
    }
}
