package com.netease;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-12.
 */
public class MysqlStreamServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("stream mysql");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();

        try
        {
            properties.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String driver = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        try
        {
            conn = DriverManager.getConnection(url, username, password);

            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT username from users");

            File f = new File("out.txt");
            OutputStream out = null;
            try
            {
                out = new FileOutputStream(f);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            while (rs.next())
            {
                InputStream ins = rs.getBinaryStream("username");
                int temp = 0;

                while (true)
                {
                    try
                    {
                        temp = ins.read();
                        if (temp == -1)
                            break;

                        out.write(temp);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                try
                {
                    out.write('\n');
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    ins.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            try
            {
                out.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            //System.out.println("Hello " + rs.getString("username"));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            }
            catch (SQLException e)
            {
                // ignore.
            }
        }

        super.init();
    }
}
