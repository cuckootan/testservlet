package com.netease;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-9.
 */
public class ServletConfigServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.print("ServletConfig test");
        out.close();
    }

    @Override
    public void init() throws ServletException
    {
        ServletConfig config = this.getServletConfig();
        String v1 = config.getInitParameter("data1");
        System.out.println("v1: " + v1);
        String v2 = config.getInitParameter("data2");
        System.out.println("v2: " + v2);

        super.init();
    }
}
