package com.netease;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-12.
 */
public class MysqlConnectionPoolServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("mysql connection pool");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();

        try
        {
            properties.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String driver = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        // 创建连接池。
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(url);
        ds.setDriverClassName(driver);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setInitialSize(5);
        ds.setMaxTotal(10);

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * from users");

            while (rs.next())
                System.out.println("Hello " + rs.getString("username"));

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            }
            catch (SQLException e)
            {
                // ignore.
            }
        }

        super.init();
    }
}
