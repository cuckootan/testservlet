package com.netease;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-11.
 */
public class ConcurrentServlet extends HttpServlet
{
    String name;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        synchronized (this)
        {
            // 还可以将实例变量定义为局部的。
            name = req.getParameter("userName");
            PrintWriter out = resp.getWriter();

            try
            {
                Thread.sleep(5000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            out.println("username: " + name);
        }
    }

    @Override
    public void destroy()
    {
        super.destroy();
    }

    @Override
    public void init() throws ServletException
    {
        super.init();
    }
}
