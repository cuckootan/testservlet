package com.netease;

/**
 * Created by cuckootan on 17-3-8.
 */

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class NoodlesServlet extends HttpServlet
{
    @Override
    public void destroy()
    {
        System.out.println("destroy method");
        super.destroy();
    }

    @Override
    public void init() throws ServletException
    {
        System.out.println("init method");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        System.out.println("doGet method");
        PrintWriter writer = resp.getWriter();

        String vegetable = req.getParameter("vegetable");

        if (vegetable == null)
            vegetable = "Default";

        writer.println("<html><body>");
        writer.println("<h1>Nooldes with " + vegetable + "</h1>");
        writer.println("</body></html>");

        writer.flush();
        writer.close();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        System.out.println("service method");
        super.service(req, resp);
    }
}
