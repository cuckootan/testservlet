package com.netease.mybatis;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-13.
 */
public class TestMybatisAnnotationServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("test mybatis annotation");
    }

    @Override
    public void init() throws ServletException
    {
        InputStream ins = TestMybatisServlet.class.getClassLoader().getResourceAsStream("mybatis/MybatisConfig.xml");
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(ins);

        // 将 mapper 加入到 conf 中。所以实质上，注解方式与一般方式是相同的。
        Configuration conf = sessionFactory.getConfiguration();
        conf.addMapper(HandleUserInfoAnnotation.class);

        SqlSession session = sessionFactory.openSession(true);

        try
        {
            HandleUserInfoAnnotation handleUserInfoAnnotation = session.getMapper(HandleUserInfoAnnotation.class);
            User user = handleUserInfoAnnotation.getUser(1);
            System.out.println(user.getId() + " " + user.getUsername() + " " + user.getCorp() + " " + user.getPassword());

            user = new User("南京大学", "NJU", "88888888");
            handleUserInfoAnnotation.addUser(user);

//            System.out.println(user.getId());

            user.setUsername("清华大学");
            user.setCorp("THU");
            user.setPassword("999");
            handleUserInfoAnnotation.updateUser(user);

            handleUserInfoAnnotation.deleteUser(user.getId());
        }
        finally
        {
            session.close();
        }

        super.init();
    }
}
