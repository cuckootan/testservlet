package com.netease.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-12.
 */
public class TestMybatisServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("test mybatis");
    }

    @Override
    public void init() throws ServletException
    {
        InputStream ins = TestMybatisServlet.class.getClassLoader().getResourceAsStream("mybatis/MybatisConfig.xml");
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(ins);
        SqlSession session = sessionFactory.openSession(true);

        try
        {
            HandleUserInfo handleUserInfo = session.getMapper(HandleUserInfo.class);

            User user = handleUserInfo.getUser(1);
            System.out.println(user.getId() + " " + user.getUsername() + " " + user.getCorp() + " " + user.getPassword());

            user = new User("南京大学", "NJU", "88888888");
            handleUserInfo.addUser(user);

//            System.out.println(user.getId());

            user.setUsername("清华大学");
            user.setCorp("THU");
            user.setPassword("999");
            handleUserInfo.updateUser(user);

            handleUserInfo.deleteUser(user.getId());
        }
        finally
        {
            session.close();
        }

        super.init();
    }
}
