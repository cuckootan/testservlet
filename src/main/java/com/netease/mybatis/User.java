package com.netease.mybatis;

/**
 * Created by cuckootan on 17-3-12.
 */
public class User
{
    private int id;
    private String username;
    private String corp;
    private String password;

    public User(Integer id, String username, String corp, String password)
    {
        this.id = id;
        this.username = username;
        this.corp = corp;
        this.password = password;
    }

    public User(String username, String corp, String password)
    {
        this.username = username;
        this.corp = corp;
        this.password = password;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getCorp()
    {
        return corp;
    }

    public void setCorp(String corp)
    {
        this.corp = corp;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
