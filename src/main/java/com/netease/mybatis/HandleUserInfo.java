package com.netease.mybatis;

/**
 * Created by cuckootan on 17-3-12.
 */
public interface HandleUserInfo
{
    User getUser(int id);
    void addUser(User user);
    void updateUser(User user);
    void deleteUser(int id);
}