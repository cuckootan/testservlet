package com.netease.mybatis;

import org.apache.ibatis.annotations.*;

/**
 * Created by cuckootan on 17-3-13.
 */
public interface HandleUserInfoAnnotation
{
    // 有了注解之后，就不需要配置 mapper 了，更不需要在 mybatis 的 config 文件中添加相应的 mapper 了。
    @Select("SELECT id, username, corp, password FROM users WHERE id = #{id}")
    User getUser(int id);
    @Insert("INSERT INTO users (username, corp, password) VALUES (#{username}, #{corp}, #{password})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void addUser(User user);
    @Update("UPDATE users SET username = #{username}, corp = #{corp}, password = #{password} WHERE id = #{id}")
    void updateUser(User user);
    @Delete("DELETE FROM users WHERE id = #{id}")
    void deleteUser(int id);
}
