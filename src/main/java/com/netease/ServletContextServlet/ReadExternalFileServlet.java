package com.netease.ServletContextServlet;


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-11.
 */
public class ReadExternalFileServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.print("Read Servlet Context in Web XML");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();

        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();
        try
        {
            properties.load(in);
            System.out.println(properties.getProperty("hello"));
            System.out.println(properties.getProperty("world"));
            System.out.println(properties.getProperty("fuck"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        super.init();
    }
}
