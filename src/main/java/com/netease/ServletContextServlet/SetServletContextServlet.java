package com.netease.ServletContextServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-9.
 */
public class SetServletContextServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.print("Set Servlet Context");
    }

    @Override
    public void init() throws ServletException
    {
        System.out.println("boot up SetServletCOntextServlet");

        // 所设置的属性为所有 servlet 共享
        ServletContext context = this.getServletContext();
        context.setAttribute("attribute1", "111");

        super.init();
    }
}
