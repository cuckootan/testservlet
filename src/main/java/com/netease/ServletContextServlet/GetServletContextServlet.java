package com.netease.ServletContextServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-9.
 */
public class GetServletContextServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.print("Get Servlet Context");
    }

    @Override
    public void init() throws ServletException
    {
        System.out.println("boot up GetServletCOntextServlet");

        ServletContext context = this.getServletContext();
        String attribute = (String)context.getAttribute("attribute1");
        System.out.println("attribute: " + attribute);

        super.init();
    }
}
