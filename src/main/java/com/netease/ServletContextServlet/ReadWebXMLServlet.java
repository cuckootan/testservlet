package com.netease.ServletContextServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cuckootan on 17-3-9.
 */
public class ReadWebXMLServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.print("Read Servlet Context in Web XML");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        String globalValue1 = context.getInitParameter("globalData1");
        String globalValue2 = context.getInitParameter("globalData2");
        System.out.println("global value1: " + globalValue1 + ", global value2:" + globalValue2);

        super.init();
    }
}
