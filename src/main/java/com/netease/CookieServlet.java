package com.netease;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by cuckootan on 17-3-10.
 */
public class CookieServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        process(req, resp);
    }

    protected void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String userName = req.getParameter("userName");
        String userPassword = req.getParameter("userPassword");
        RequestDispatcher dispatcher = null;

        // 设置 cookie，并添加到 response 中。
        Cookie userNameCookie = new Cookie("userName", userName);
        Cookie pwdCookie = new Cookie("pwd", userPassword);
        // 默认有效期是会话结束。
        // 2 min。
        userNameCookie.setMaxAge(2 * 60);
        pwdCookie.setMaxAge(2 * 60);
        resp.addCookie(userNameCookie);
        resp.addCookie(pwdCookie);

        // 获取 cookie。
        Cookie[] cookies = req.getCookies();
        if (cookies != null)
        {
            for (Cookie cookie: cookies)
            {
                if (cookie.getName().equals("userName"))
                    userName = cookie.getValue();
                if (cookie.getName().equals("pwd"))
                    userPassword = cookie.getValue();
            }
        }

        if (userName.equals("hello") && userPassword.equals("123"))
        {
            resp.setContentType("text/html;charset=UTF-8");
            PrintWriter writer = resp.getWriter();

            writer.println("<html>");
            writer.println("    <head><title>用户中心</title></head>");
            writer.println("    <body>");
            writer.println("用户名： " + userName);
            writer.println("<br></br>");
            writer.println("用户密码： " + userPassword);
            writer.println("    </body>");
            writer.println("</html>");
            writer.close();
        }
        else
        {
            dispatcher = req.getRequestDispatcher("/error.html");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        System.out.println("A Post Request");

        super.service(req, resp);
    }
}
