package com.netease;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * Created by cuckootan on 17-3-11.
 */
public class TestListener implements HttpSessionAttributeListener, ServletContextListener, ServletRequestListener
{
    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        System.out.println("listener: context init");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        System.out.println("listener: context destroy");
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre)
    {
        System.out.println("listener: request destroy");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre)
    {
        System.out.println("listener: init request");
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent se)
    {
        System.out.println("listener: add attribute");
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent se)
    {
        System.out.println("listener: remove attribute");
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent se)
    {
        System.out.println("listener: replace attribute");
    }
}
