package com.netease;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by cuckootan on 17-3-11.
 */
public class TestFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        System.out.println("init filter");

        String value = filterConfig.getInitParameter("filterParam");
        System.out.println("filter config param: " + value);
    }

    @Override
    public void destroy()
    {
        System.out.println("destroy filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        System.out.println("doFilter");

        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpSession session = req.getSession();
        if (session.getAttribute("userName") == null)
        {
            HttpServletResponse resp = (HttpServletResponse)servletResponse;
            resp.sendRedirect("test_session.html");
        }
        else
        {
            System.out.println(session.getAttribute("userName"));
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
