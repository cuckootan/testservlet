package com.netease;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;

/**
 * Created by cuckootan on 17-3-12.
 */
public class MysqlBatchServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter out = resp.getWriter();
        out.println("batch mysql");
    }

    @Override
    public void init() throws ServletException
    {
        ServletContext context = this.getServletContext();
        InputStream in = context.getClassLoader().getResourceAsStream("jdbc/JDBC.properties");
        Properties properties = new Properties();

        try
        {
            properties.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String driver = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        Connection conn = null;
        Statement stmt = null;

        try
        {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        try
        {
            conn = DriverManager.getConnection(url, username, password);
            stmt = conn.createStatement();

            // 批量 SQL 语句。
            stmt.addBatch("INSERT INTO users (username) VALUES (\"南京大学\")");
            stmt.addBatch("INSERT INTO users (username) VALUES (\"清华大学\")");

            stmt.executeBatch();
            stmt.clearBatch();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
            }
            catch (SQLException e)
            {
                // ignore.
            }
        }

        super.init();
    }
}
